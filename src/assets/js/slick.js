$(document).ready(function(){
    $('.intro__slider').slick({
        dots: true,
        arrows:false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 5000,
      });
});
